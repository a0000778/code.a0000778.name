Loader={
	'css': function(path){
		console.log('Load CSS file '+path);
		document.head.$add('link',{
			'type': 'text/css',
			'rel': 'stylesheet',
			'href': path
		});
	},
	'js': function(path,onload){
		console.log('Load JS file '+path);
		if(onload){
			document.body.$add('script',{
				'src': path
			}).addEventListener('load',onload);
		}else{
			document.body.$add('script',{
				'src': path
			});
		}
	},
	'page': function(page,divid){
		if(!page) return;
		console.log('Load page '+page);
		if(!divid) location.hash=page;
		else{
			var s=location.hash.indexOf(divid+'=');
			if(s>=0){
				var e=location.hash.indexOf('!',s);e=(e>=0? e-1:location.hash.length);
				location.hash=location.hash.replace(location.hash.substring(s,e),divid+'='+page);
			}else{
				location.hash+='!'+divid+'='+page;
			}
		}
		$((divid? divid:'main')).innerHTML=ajaxpost('page/'+page+'.html?'+pagever);
		window.scrollTo(0,0);
		var scantag=function(tagname,func){
			var dom=document.getElementsByTagName(tagname);
			var i=0,doms=[];
			while(i<dom.length){
				doms[doms.length]=dom[i];
				i++;
			}
			doms.forEach(func);
		}
		scantag('code',function(i){//程式碼高亮
			var cn=i.className.split(' ');
			Loader.plugin.SyntaxHighlighter.lang(cn[cn.indexOf('brush:')+1]);
		});
		scantag('decode', function(i){//加密資料解碼
			var data=JSON.parse(i.innerHTML);
			i.outerHTML=decode(data.v,data.k);
		});
		scantag('run',function(i){//程式碼實行
			i.$replace('iframe',{'src':'data:text/html; charset=utf-8,'+encodeURI(i.innerHTML)});
		});
		scantag('img', function(i){//圖片自適應大小
			i.addEventListener('load',function(){
				if(this.width>window.width()-100){
					this.style.width=(window.width()-100)+'px';
				}
			});
		});
	},
	'plugin': {
		'SyntaxHighlighter': {
			'path': 'plugin/SyntaxHighlighter/',
			'include': [],
			'loaded': 0,
			'lang': function(lang){
				if(!this.include.length){
					console.log('Plugin SyntaxHighlighter: Load Core');
					Loader.css(this.path+'styles/shCoreEclipse.css');
					Loader.css(this.path+'styles/shThemeEclipse.css');
					Loader.js(this.path+'scripts/shCore.js',function(){
						SyntaxHighlighter.defaults.toolbar=false;
						SyntaxHighlighter.config.tagName='code';
						Loader.plugin.SyntaxHighlighter.include.forEach(function(i){
							console.log('Plugin SyntaxHighlighter: Load lang '+i);
							Loader.js(Loader.plugin.SyntaxHighlighter.path+'scripts/shBrush'+i+'.js',function(){
								Loader.plugin.SyntaxHighlighter.loaded++;
								if(Loader.plugin.SyntaxHighlighter.loaded==Loader.plugin.SyntaxHighlighter.include.length){
									console.log('Plugin SyntaxHighlighter: Loaded');
									SyntaxHighlighter.highlight();
								}
							});
						});
					});
				}
				var langname=false;
				switch(lang.toLowerCase()){
					case 'bash': case 'shell': case 'sh':
						langname='Bash';break;
					case 'css':
						langname='Css';break;
					case 'js': case 'javascript':
						langname='JScript';break;
					case 'php':
						langname='Php';break;
					case 'sql':
						langname='Sql';break;
					case 'html': case 'xml':
						langname='Xml';break;
					case 'text': case 'plain':
						langname='Plain';break;
				}
				if(langname){
					if(this.include.indexOf(langname)==-1){
						this.include[this.include.length]=langname;
						if(this.include.length && this.loaded){
							location.reload();
						}
					}else if(this.loaded>=this.include.length){
						SyntaxHighlighter.highlight();
					}
					return true;
				}
				return false;
			}
		}
	}
}
pagever='44';
window.addEventListener('load',function(){
	Loader.css('style/common.css');
	if(location.hash){
		var hashs=location.hash.substring(1,location.hash.length).split('!');
		var i=0,v;
		while(i<hashs.length){
			v=hashs[i].split('=');
			((v.length>1)? Loader.page(v[1],v[0]):Loader.page(v[0]));
			i++;
		}
	}else{
		Loader.page('home');
	}
	//Loader.page(((location.hash)? location.hash.substring(1,location.hash.indexOf('!')):'home'));
});
