function $(select){
	return arguments.length? document.getElementById(select):document.body;
}
$.tag=function(tagname, attr){
	var ele=document.createElement(tagname);
	for(var k1 in attr){
		if(typeof attr[k1] === 'object'){
			for(var k2 in attr[k1]){
				ele[k1][k2]=attr[k1][k2];
			}
		}else{
			ele[k1]=attr[k1];
		}
	}
	return ele;
}
HTMLElement.prototype.$add=function(){
	var at=0;
	var ele=('string'===typeof arguments[at])? $.tag(arguments[at++],arguments[at++]):arguments[at++];
	if(arguments[at]) this.insertBefore(ele,arguments[at++]);
	else this.appendChild(ele);
	return (arguments[++at]? this:ele);
}
HTMLElement.prototype.$del=function(returnParent){
	this.parentNode.removeChild(this);
	return (returnParent? this.parentNode:this);
}
HTMLElement.prototype.$replace=function(){
	var at=0;
	var ele=('string'===typeof arguments[at])? $.tag(arguments[at++],arguments[at++]):arguments[at++];
	this.parentNode.replaceChild(ele,this);
	return (arguments[at]? this.parentNode:this);
}

window.width=function(){
	return window.innerWidth? window.innerWidth:document.body.clientWidth;
}
window.height=function(){
	return window.innerHeight? window.innerHeight:document.body.clientHeight;
}

function Ajax(method, url, data, option){
	this.xhr=new XMLHttpRequest();
	this.method=method.toUpperCase();
	this.url=url;
	this.data=data? data:null;
	var option=option? option:{};
	this.option={
		'async': option.hasOwnProperty('async')? option.async:true,
		'auth': option.hasOwnProperty('auth')? {
			'user': option.auth.hasOwnProperty('user')? option.auth.user:null,
			'pass': option.auth.hasOwnProperty('pass')? option.auth.pass:null
		}:{},
		'formData': option.hasOwnProperty('formData')? formData:true
	};
}
Ajax.prototype.setHeader=function(header,value){
	this.xhr.setRequestHeader(header,value);
	return this;
}
Ajax.prototype.on=function(eventName,func){
	this.xhr.addEventListener(eventName,func.bind(this));
	return this;
}
Ajax.prototype.send=function(){
	if(this.method=='GET' || !this.option.formData){
		var data='';
		var s='';
		for(k in this.data){
			if('object'==typeof this.data[k]){
				data+=s+this.data[k].reduce(function(r,v){
					r.r+=r.s+r.k+'='+encodeURIComponent(v);
					r.s='&';
				},{'r':'','s':'','k':k+'[]'}).r;
			}else{
				data+=s+k+'='+encodeURIComponent(this.data[k]);
			}
			s='&';
		}
	}else{
		var data=new FormData();
		for(k in this.data){
			if('object'==typeof this.data[k]){
				this.data[k].forEach(function(k,v){
					this.append(k+'[]',v);
				}.bind(data,k));
			}else{
				data.append(k,this.data[k]);
			}
		}
	}
	this.xhr.open(
		this.method,
		this.url+(this.method=='GET'? (this.url.indexOf('?')>=0? '&':'?')+data:''),
		this.option.async,
		this.option.auth.user,
		this.option.auth.pass
	);
	this.xhr.send(this.method=='GET'? null:data);
	return this;
}
Ajax.prototype.abort=function(){
	this.xhr.abort();
	return this;
}
Ajax.prototype.result=function(forceText){
	if(this.xhr.readyState!==4) return null;
	return forceText? this.xhr.responseText:this.xhr.response;
}
Ajax.prototype.resultHeader=function(select){
	if(this.xhr.readyState!==4) return null;
	if(select) return this.xhr.getResponseHeader(select);
	else return this.xhr.getAllResponseHeaders().split('\n').reduce(function(r,v){
		if(!v.length) return r;
		var s=v.indexOf(':');
		r[v.substring(0,s)]=v.substring(s+2,v.length);
		return r;
	},{});
}
function ajaxget(url,data){
	return new Ajax('GET',url,data,{'async': false}).send().result();
}
function ajaxpost(url,data){
	return new Ajax('POST',url,data,{'async': false}).send().result();
}

function decode(t,k){
	var r=0;
	var re='';
	var key=[];
	var its='0123456789ABCDEF';
	
	for(var c=0;c<k.length;c+=2){
		key[key.length]=(k.charCodeAt(c)<<8)+(k.charCodeAt(c+1)? k.charCodeAt(c+1):0xFF);
	}
	for(var c=0;c<t.length;c+=4){
		r=its.indexOf(t.charAt(c));r=r<<4;
		r+=its.indexOf(t.charAt(c+1));r=r<<4;
		r+=its.indexOf(t.charAt(c+2));r=r<<4;
		r+=its.indexOf(t.charAt(c+3));
		re+=String.fromCharCode(r^key[(c/4)%key.length]);
	}
	return re;
}
